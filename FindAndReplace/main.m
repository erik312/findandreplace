//
//  main.m
//  FindAndReplace
//
//  Created by Clique Studios on 1/29/14.
//  Copyright (c) 2014 312 Development. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}

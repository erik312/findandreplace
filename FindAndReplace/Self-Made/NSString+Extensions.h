//
//  NSString+Extensions.h
//  FindAndReplace
//
//  Created by Clique Studios on 2/6/14.
//  Copyright (c) 2014 312 Development. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extensions)

+ (int)toInt;
+ (long)toLong;
+ (float)toFloat;
+ (bool)toBool;
+ (NSString *)containsString:(NSString *)subString;
+ (NSString *)md5;
+ (NSString *)base64encode;
+ (NSString *)base64decode;
- (NSString *)URLEncode;
- (NSString *)URLDecode;
- (id)JSONForResourceName:(NSString *)resourceName;

/** Trims whitespace and newlines from the ends of the string */
- (NSString *)trim;

/** Trim the ends of the string with the characters in the provided string */
- (NSString *)trim:(NSString *)characters;

@end

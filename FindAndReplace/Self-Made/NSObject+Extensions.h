//
//  NSObject+Extensions.h
//  FindAndReplace
//
//  Created by Clique Studios on 2/6/14.
//  Copyright (c) 2014 312 Development. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Extensions)

// @source https://github.com/yaakaito/Overline
- (BOOL)isNullObject;
- (BOOL)isArray;
- (BOOL)isDictionary;
- (BOOL)isSet;
- (BOOL)isString;
- (BOOL)isNumber;

@end

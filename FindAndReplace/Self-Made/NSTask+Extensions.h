//
//  NSTask+Extensions.h
//  FindAndReplace
//
//  Created by Clique Studios on 2/6/14.
//  Copyright (c) 2014 312 Development. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSTask (Extensions)

+ (void)exec;
+ (void)exec:(NSArray *)withArgs;

@end

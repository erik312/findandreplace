//
//  NSNumber+Extensions.h
//  FindAndReplace
//
//  Created by Clique Studios on 2/6/14.
//  Copyright (c) 2014 312 Development. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (Extensions)

- (BOOL)isEqualToInt:(int)value;
- (BOOL)isEqualToInteger:(NSInteger)value;
- (BOOL)isEqualToUnsignedInt:(unsigned int)value;
- (BOOL)isEqualToUnsignedInteger:(NSUInteger)value;
- (BOOL)isEqualToLong:(long)value;
- (BOOL)isEqualToLongLong:(long long)value;
- (BOOL)isEqualToUnsignedLong:(unsigned long)value;
- (BOOL)isEqualToUnsignedLongLong:(unsigned long long)value;
- (BOOL)isEqualToDouble:(double)value;
- (BOOL)isEqualToFloat:(float)value;

@end

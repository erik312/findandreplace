//
//  NSString+Extensions.m
//  FindAndReplace
//
//  Created by Clique Studios on 2/6/14.
//  Copyright (c) 2014 312 Development. All rights reserved.
//

#import "NSString+Extensions.h"

@implementation NSString (Extensions)

- (NSString *)URLEncode {
    return [self stringByEncodingURL];
}

- (NSString *)stringByEncodingURL {
    return (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(NULL, (__bridge CFStringRef)self, NULL, (__bridge CFStringRef)@"!*'();:@&=+$.,/?%#[]", kCFStringEncodingUTF8);
}

- (NSString *)URLDecode {
    return [self stringByDecodingURL];
}

- (NSString *)stringByDecodingURL {
    return (__bridge_transfer NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL, (__bridge CFStringRef)self, CFSTR(""), kCFStringEncodingUTF8);
}

- (NSString *)trim {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString *)trim:(NSString *)characters
{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:characters]];
}
- (id)JSONForResourceName:(NSString *)resourceName {
    NSData *jsonData = [NSData dataWithContentsOfFile:[self pathForResource:resourceName ofType:@"json"]];
    return [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:NULL];
}


@end

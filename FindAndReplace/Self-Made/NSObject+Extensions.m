//
//  NSObject+Extensions.m
//  FindAndReplace
//
//  Created by Clique Studios on 2/6/14.
//  Copyright (c) 2014 312 Development. All rights reserved.
//

#import "NSObject+Extensions.h"

@implementation NSObject (Extensions)

- (BOOL)isNullObject {
    // @source https://github.com/yaakaito/Overline
    return [self isEqual:[NSNull null]];
}

- (BOOL)isArray {
    // @source https://github.com/yaakaito/Overline
    return [self isKindOfClass:[NSArray class]];
}

- (BOOL)isDictionary {
    // @source https://github.com/yaakaito/Overline
    return [self isKindOfClass:[NSDictionary class]];
}

- (BOOL)isSet {
    // @source https://github.com/yaakaito/Overline
    return [self isKindOfClass:[NSSet class]];
}

- (BOOL)isString {
    // @source https://github.com/yaakaito/Overline
    return [self isKindOfClass:[NSString class]];
}

- (BOOL)isNumber {
    // @source https://github.com/yaakaito/Overline
    return [self isKindOfClass:[NSNumber class]];
}

@end

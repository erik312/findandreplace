//
//  NSArray+Extensions.h
//  FindAndReplace
//
//  Created by Clique Studios on 2/6/14.
//  Copyright (c) 2014 312 Development. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Extensions)

- (NSArray *)difference:(NSArray *)array;
- (NSArray *)arrayDifferenceWithArray:(NSArray *)array;
- (NSArray *)unionise:(NSArray *)array;
- (NSArray *)arrayByUnionisingArray:(NSArray *)array;

@end

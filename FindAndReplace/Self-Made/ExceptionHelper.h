//
//  ExceptionHelper.h
//  FindAndReplace
//
//  Created by Clique Studios on 2/6/14.
//  Copyright (c) 2014 312 Development. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExceptionHelper : NSObject

// @source https://github.com/enormego/cocoa-helpers
+ (void)generateStackTraceForException:(NSException*)exception;

@end

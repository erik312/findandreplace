//
//  NSNumber+Extensions.m
//  FindAndReplace
//
//  Created by Clique Studios on 2/6/14.
//  Copyright (c) 2014 312 Development. All rights reserved.
//

#import "NSNumber+Extensions.h"

@implementation NSNumber (Extensions)

- (BOOL)isEqualToInt:(int)value {
    return [self intValue] == value;
}

- (BOOL)isEqualToInteger:(NSInteger)value {
    return [self integerValue] == value;
}

- (BOOL)isEqualToUnsignedInt:(unsigned int)value {
    return [self unsignedIntValue] == value;
}

- (BOOL)isEqualToUnsignedInteger:(NSUInteger)value {
    return [self unsignedIntegerValue] == value;
}

- (BOOL)isEqualToLong:(long)value {
    return [self longValue] == value;
}

- (BOOL)isEqualToLongLong:(long long int)value {
    return [self longLongValue] == value;
}

- (BOOL)isEqualToUnsignedLong:(unsigned long)value {
    return [self unsignedLongValue] == value;
}

- (BOOL)isEqualToUnsignedLongLong:(unsigned long long)value {
    return [self unsignedLongLongValue] == value;
}

- (BOOL)isEqualToDouble:(double)value {
    return [self doubleValue] == value;
}

- (BOOL)isEqualToFloat:(float)value {
    return [self floatValue] == value;
}

@end

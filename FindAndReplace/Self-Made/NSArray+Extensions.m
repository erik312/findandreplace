//
//  NSArray+Extensions.m
//  FindAndReplace
//
//  Created by Clique Studios on 2/6/14.
//  Copyright (c) 2014 312 Development. All rights reserved.
//

#import "NSArray+Extensions.h"

@implementation NSArray (Extensions)

- (NSArray *)difference:(NSArray *)array {
    return [self arrayDifferenceWithArray:array];
}
- (NSArray *)arrayDifferenceWithArray:(NSArray *)array {
    NSMutableOrderedSet *all = [NSMutableOrderedSet orderedSetWithArray:[self arrayByAddingObjectsFromArray:array]];
    NSMutableOrderedSet *dup = [NSMutableOrderedSet orderedSetWithArray:self];
    [dup intersectOrderedSet:[NSOrderedSet orderedSetWithArray:array]];
    [all minusOrderedSet:dup];
    return [all array];
}
- (NSArray *)unionise:(NSArray *)array {
    return [self arrayByUnionisingArray:array];
}
- (NSArray *)arrayByUnionisingArray:(NSArray *)array {
    NSMutableOrderedSet *base = [NSMutableOrderedSet orderedSetWithArray:self];
    [base unionOrderedSet:[NSOrderedSet orderedSetWithArray:array]];
    return [base array];
}

@end

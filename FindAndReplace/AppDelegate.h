//
//  AppDelegate.h
//  FindAndReplace
//
//  Created by Clique Studios on 1/29/14.
//  Copyright (c) 2014 312 Development. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
